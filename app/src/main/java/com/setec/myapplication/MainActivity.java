package com.setec.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.time.LocalDate;

public class MainActivity extends AppCompatActivity implements UserAdapter.OnClickListener {
    private    NoteData db;
    private      NoteDao  dao;
    private     UserAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recycle = findViewById(R.id.recycle);
   db =
               Room.databaseBuilder(this, NoteData.class, "MyDatabase3").


               allowMainThreadQueries().build();



             dao  = db.notedao();
//        dao.deleteall();
//        dao.insert(new Note("Birthday","Subtitle"));


        Log.d("tag","message "+dao.toString());

   adapter = new UserAdapter(this,        dao.getAll(),this);
        recycle.setLayoutManager(new LinearLayoutManager(this));
        recycle.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.addmenu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(this,"Click",Toast.LENGTH_SHORT).show();
        loadpopup();
        return super.onOptionsItemSelected(item);
    }

    private void loadpopup() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.customdialog);
        Button btn = dialog.findViewById(R.id.btn_save);
        EditText txt1,txt2 ;
        txt1 =  dialog.findViewById(R.id.notetitle);
        txt2 = dialog.findViewById(R.id.notedesc);
        dialog.show();


  btn.setOnClickListener(new View.OnClickListener() {
      @RequiresApi(api = Build.VERSION_CODES.O)
      @Override
      public void onClick(View v) {
//          if(txt1.getText().toString().isEmpty() || txt2.getText().toString().isEmpty()){
////          AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
////          builder.setTitle("Empty").setNegativeButton("Comfirm",(dialog1, which) -> {
////                dialog1.dismiss();
////          });
////          builder.create();
////          builder.show();
//              dialog.dismiss();
//              return;
//          }
          dao.insert(new Note(   txt1.getText().toString(),txt2.getText().toString(),true, LocalDate.now().toString()));
          adapter.addnewitem(dao.getAll());
          dialog.dismiss();
      }
  });

        ///new datta



    }

    private void loadedit(Note note) {
        Dialog dialog2 = new Dialog(this);
        dialog2.setContentView(R.layout.customdialog);
        Button btn = dialog2.findViewById(R.id.btn_save);
        EditText txt1,txt2 ;
        txt1 = dialog2.findViewById(R.id.notetitle);
        txt2 =  dialog2.findViewById(R.id.notedesc);
        btn.setText("Modify");
        txt1.setText(note.getTitle().toString());
        txt2.setText(note.getDescription().toString());
        dialog2.show();

//
//
        btn.setOnClickListener(v -> {
                note.setTitle(txt1.getText().toString());
                note.setDescription(txt2.getText().toString());

            dao.update(note);
            adapter.addnewitem(dao.getAll());
            dialog2.dismiss();
        });

        ///new datta



    }
    @Override
    public void click(Note note) {
            dao.delete(note);
            adapter.addnewitem(dao.getAll());

    }

    @Override
    public void edit(Note note) {
        loadedit(note);

    }

    @Override
    public void done(Note note) {
        if(note.getIsactive() == true){
            note.setIsactive(false);
        }
        else{
            note.setIsactive(true);
        }


        dao.update(note);
        adapter.addnewitem(dao.getAll());
    }
}