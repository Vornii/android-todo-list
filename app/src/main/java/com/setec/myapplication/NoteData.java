package com.setec.myapplication;

import androidx.room.Database;
import androidx.room.RoomDatabase;


@Database(entities = {Note.class}, version = 1)

public abstract class NoteData extends RoomDatabase {
        public  abstract NoteDao notedao ();


}
