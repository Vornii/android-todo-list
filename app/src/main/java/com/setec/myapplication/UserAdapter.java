package com.setec.myapplication;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;

import java.util.List;

public class UserAdapter extends  RecyclerView.Adapter<UserAdapter.UserHolder> {
    Context context;
   private List<Note> item;

    OnClickListener listener;



    public UserAdapter(Context context, List<Note> item, OnClickListener listener) {
        this.context = context;
        this.item = item;
        this.listener = listener;
    }

    public List<Note> get_item() {
        return item;
    }

    public void set_item(List<Note> _item) {
        this.item = _item;
    }

    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.item_note,parent,false);
        return new UserHolder(view);
    }
    public void addnewitem(List<Note> items){
//       this.item.clear();
        this.item.clear();
        this.item = items;
        notifyDataSetChanged();
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
        Note note = item.get(position);
            holder.txtdesc.setText(note.getDescription());
            holder.txttitle.setText(note.getTitle());
            Boolean t = note.getIsactive();

            if(t == true ){
                holder.txtactive.setText("Active");

                holder.icon.setImageResource(R.drawable.ic_baseline_circle_24);
            }
            else{

                holder.txtactive.setText("In active");

                holder.icon.setImageResource(R.drawable.icon_inactive);
            }

            holder.btndelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                        listener.click(note);
                    holder.s.close();

                }
            });
            holder.btndone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.done(note);
                    holder.s.close();
                }
            });
            holder.btnedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.edit(note);
                    holder.s.close();
                }
            });




    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class UserHolder extends RecyclerView.ViewHolder {
            TextView txttitle , txtdesc,txtactive;
            ImageView btnedit,btndelete, btndone, icon;
            SwipeLayout s ;

        public UserHolder(@NonNull View itemView) {

            super(itemView);
            txttitle = itemView.findViewById(R.id.txttitle);
            txtdesc = itemView.findViewById(R.id.txtdesc);
            txtactive = itemView.findViewById(R.id.txtactive);
            btnedit = itemView.findViewById(R.id.img_btn_edit);
            btndelete = itemView.findViewById(R.id.img_btn_delete);
            btndone = itemView.findViewById(R.id.img_btn_done);
            icon = itemView.findViewById(R.id.iconstatus);
            s = itemView.findViewById(R.id.layout_swipe);



        }
    }


    public interface OnClickListener  {

          void click(Note note);

          void edit(Note note);

          void done(Note note);


    }
}
